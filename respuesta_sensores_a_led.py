# Respuesta sensores a led

# Cada sensor mide 6x6mm y los centros de estos están separados a
# 7mm, por loq ue entre sensor y sensor hay 1mm.

# Posiciones de los centro de los sensores


import numpy as np
import math
from matplotlib.pyplot import *
import matplotlib.pyplot as plt

pos_cen_sen_x = np.array([4,11,18,25,32,39,46,53,
                 4,11,18,25,32,39,46,53,
                 4,11,18,25,32,39,46,53,
                 4,11,18,25,32,39,46,53,
                 4,11,18,25,32,39,46,53,
                 4,11,18,25,32,39,46,53,
                 4,11,18,25,32,39,46,53,
                 4,11,18,25,32,39,46,53])

pos_cen_sen_y = np.array([4,4,4,4,4,4,4,4,
                 11,11,11,11,11,11,11,11,
                 18,18,18,18,18,18,18,18,
                 25,25,25,25,25,25,25,25,
                 32,32,32,32,32,32,32,32,
                 39,39,39,39,39,39,39,39,
                 46,46,46,46,46,46,46,46,
                 53,53,53,53,53,53,53,53])


pos_led_x = np.array([0, 57, 0, 57])
pos_led_y = np.array([0, 0, 57, 57])

# distancia del led al centro de cada sensor (1,2,3,4) de arriba izquierda
# en sentido horario

distancia_1 = np.sqrt(np.absolute(pos_cen_sen_y)**2 + np.absolute(pos_cen_sen_x)**2)
distancia_2 = np.sqrt(np.absolute(pos_cen_sen_y)**2 + np.absolute(pos_cen_sen_x - 57)**2)
distancia_3 = np.sqrt(np.absolute(pos_cen_sen_y - 57)**2 + np.absolute(pos_cen_sen_x - 57)**2)
distancia_4 = np.sqrt(np.absolute(pos_cen_sen_y - 57)**2 + np.absolute(pos_cen_sen_x)**2)


# Calculamos el ángulo sólido del sensor = S*cos\theta /R^2

#Primero definimos el ángulo con el que cada sensor ve el led (1,2,3,4)

theta_1 = np.arcsin(pos_cen_sen_x/distancia_1)
theta_2 = -np.arcsin((pos_cen_sen_x - 57)/distancia_2)
theta_3 = -np.arcsin((pos_cen_sen_x - 57)/distancia_3)
theta_4 = np.arcsin(pos_cen_sen_x/distancia_4)

# ángulo solido: file:///C:/Users/Nerea/Downloads/ANGSOLIDOSOLDOVIERI-VILORIA.pdf

angulo_solido_1 = (36*theta_1/distancia_1)/(4*math.pi)
angulo_solido_2 = (36*theta_2/distancia_2)/(4*math.pi)
angulo_solido_3 = (36*theta_3/distancia_3)/(4*math.pi)
angulo_solido_4 = (36*theta_4/distancia_4)/(4*math.pi)

#Intensidad que llega a cada sensores

#Depende del ángulo sólido que cada sensor ve los leds.
# Será la suma de las intensidades de los 4 leds

Io = 1;

I = Io * (angulo_solido_1 + angulo_solido_2 + angulo_solido_3 + angulo_solido_4)

I_sensor = np.array([[0.512431126678933,	0.431184292557797,	0.361762521385747,	0.334961000827677,	0.334961000827677,	0.361762521385747,	0.431184292557797,	0.512431126678933],
[0.197991321882242,	0.274365809928989,	0.285478942232740,	0.284549821852055,	0.284549821852055,	0.285478942232740,	0.274365809928989,	0.197991321882242],
[0.145848538664130,	0.204610768050943,	0.233947514005287,	0.244901690081311,	0.244901690081311,	0.233947514005287,	0.204610768050943,	0.145848538664130],
[0.131793468010170,	0.179452403339793,	0.210080475454998,	0.224173983413985,	0.224173983413985,	0.210080475454998,	0.179452403339793,	0.131793468010170],
[0.131793468010170,	0.179452403339793,	0.210080475454998,	0.224173983413985,	0.224173983413985,	0.210080475454998,	0.179452403339793,	0.131793468010170],
[0.145848538664130,	0.204610768050943,	0.233947514005287,	0.244901690081311,	0.244901690081311,	0.233947514005287,	0.204610768050943,	0.145848538664130],
[0.197991321882242,	0.274365809928989,	0.285478942232740,	0.284549821852055,	0.284549821852055,	0.285478942232740,	0.274365809928989,	0.197991321882242],
[0.512431126678933,	0.431184292557797,	0.361762521385747,	0.334961000827677,	0.334961000827677,	0.361762521385747,	0.431184292557797,	0.512431126678933]])


#Representamos las posiciones de los sensores y leds, junto con la intensidad

def extents(f):
  delta = f[1] - f[0]
  return [f[0] - delta/2, f[-1] + delta/2]

plt.imshow(I_sensor, aspect='auto', interpolation='none',
           extent=extents(pos_cen_sen_x) + extents(pos_cen_sen_y), origin='lower')


plot(pos_cen_sen_x,pos_cen_sen_y,'.k')

plot(pos_led_x,pos_led_y,'.b')
xlabel('distancia (mm)')
ylabel('distancia (mm)')
